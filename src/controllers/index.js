const User = require("../models/userModel");
const Cow = require("../models/cowModels");
const sendToken = require("../../utils/sendLoginToken");
const cloudinary = require("cloudinary").v2;
const EvenModel = require("../models/eventsModel");
exports.registerUser = async (req, res, next) => {
    try {
        const user = await User.create({ ...req.body });

        if (!user) res.status(404).json({ message: "Can't create user" });

        res.status(200).json({ message: "successs", user });
    } catch (err) {
        res.status(404).json({ error: err });
    }
};

exports.login = async (req, res, next) => {
    try {
        const { email, password } = req.body;

        if (!email || !password) {
            res.status(404).json({
                message: "Please Enter Email And Password",
            });
        }

        const user = await User.findOne({
            email,
        });

        if (!user) {
            res.status(404).json({
                message: "Invalid Email Or Password",
            });
        }

        if (!(await user.comparePassword(password))) {
            res.status(404).json({
                message: "Invalid Password",
            });
        }
        sendToken(user, 200, res);
    } catch (err) {
        res.status(404).json({ error: err });
    }
};

exports.getUserInfo = async (req, res, next) => {
    try {
        if (!req.user?.id)
            res.status(403).json({
                message: "Please Login First",
            });

        const user = await User.findOne({
            _id: req.user._id,
        });

        if (!user)
            res.status(404).json({
                message: "Something went wrong",
            });
        const secureUser = user.toObject();
        delete secureUser.password;
        JSON.stringify(secureUser);

        res.status(200).json({
            success: true,
            user: secureUser,
        });
    } catch (error) {
        res.status(404).json({ error: err });
    }
};

exports.addCow = async (req, res, next) => {
    try {
        const { name, age, color, image } = req.body;

        if (!name || !age || !color || !image) {
            res.status(404).json({
                message: "No Field Can't Be Empty",
            });
        }

        const myCloud = await cloudinary.uploader.upload(req.body.image, {
            folder: "avatars",
        });

        const cow = await Cow.create({ ...req.body });
        cow.avatar = {
            public_id: myCloud.public_id,
            url: myCloud.secure_url,
        };

        await cow.save();

        res.status(200).json({
            success: true,
            message: "This cow is added successfully",
        });
    } catch (error) {
        res.status(404).json({ error });
    }
};

exports.getCows = async (req, res, next) => {
    try {
        const cows = await Cow.find();
        if (!cows)
            res.status(404).json({
                message: "No Cow Available",
            });
        res.status(200).json({
            success: true,
            cows,
        });
    } catch (error) {
        res.status(404).json({ error });
    }
};

exports.getSingleCow = async (req, res, next) => {
    try {
        const _id = req.params.id;

        const cow = await Cow.findById(_id).populate("events");

        if (!cow)
            res.status(404).json({
                message: "Cow Doesn't exist",
            });
        res.status(200).json({
            success: true,
            cow,
        });
    } catch (error) {
        res.status(404).json({ error });
    }
};

exports.addEvent = async (req, res, next) => {
    try {
        const { cowId, eventName, startDate, endDate, description } = req.body;

        if (!eventName || !startDate || !endDate || !description) {
            res.status(404).json({
                message: "No Field Can't Be Empty",
            });
        }

        const event = await EvenModel.create({ ...req.body });

        const cow = await Cow.findById(cowId);
        cow.events.push(event._id);
        await cow.save();

        res.status(200).json({
            success: true,
            event,
            message: "Event is added successfully",
        });
    } catch (error) {
        res.status(404).json({ error });
    }
};
