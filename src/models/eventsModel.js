const mongoose = require("mongoose");

const { Schema } = mongoose;

const eventModel = new Schema(
    {
        eventName: {
            type: String,
            required: true,
        },

        startDate: {
            type: Date,
            required: true,
        },
        endDate: {
            type: Date,
            required: true,
        },
        description: String,
    },
    { timestamps: true }
);

module.exports = mongoose.model("EvenModel", eventModel);
