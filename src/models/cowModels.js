const mongoose = require("mongoose");

const { Schema } = mongoose;

const cowSchema = new Schema(
    {
        name: {
            type: String,
            index: true,
            unique: true,
            required: true,
        },
        age: Number,
        color: String,
        avatar: {
            public_id: {
                type: String,
            },
            url: {
                type: String,
            },
        },
        isAlive: Boolean,
        events: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: "EvenModel",
            },
        ],
    },
    { timestamps: true }
);
const Cow = mongoose.model("Cow", cowSchema);

Cow.createIndexes();
module.exports = Cow;
