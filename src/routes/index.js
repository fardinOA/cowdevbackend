const express = require("express");
const { isAuth } = require("../../middlewares/auth");
const {
    registerUser,
    login,
    getUserInfo,
    addCow,
    getCows,
    getSingleCow,
    addEvent,
} = require("../controllers");
const router = express.Router();

router.post("/register", registerUser);
router.post("/login", login);
router.post("/addCow", isAuth, addCow);
router.post("/addEvent", isAuth, addEvent);
router.get("/loadUser", isAuth, getUserInfo);
router.get("/cows", isAuth, getCows);
router.get("/cow/:id", getSingleCow);

module.exports = router;
