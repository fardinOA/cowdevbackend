const jwt = require("jsonwebtoken");
const User = require("../src/models/userModel");

const isAuth = async (req, res, next) => {
    try {
        console.log(req.headers.cookies);
        // console.log(req);
        // const token = req.headers?.cookies;
        const { cowdevtoken } = req.cookies;
        // const cowdevtoken = token ? token : cowdevtoken;
        if (!cowdevtoken) {
            res.status(403).json({
                message: "Please Login First",
            });
        }

        const decodedData = jwt.verify(cowdevtoken, process.env.JWT_SECRET);

        req.user = await User.findById(decodedData.id);

        next();
    } catch (error) {
        console.log(error);
    }
};

// const authRole = (...roles) => {
//     return (req, res, next) => {
//         if (!roles.includes(req.user.role)) {
//             return next(
//                 new ErrorHandeler(
//                     `Role: ${req.user.role} is not allowed to access this resource`,
//                     403
//                 )
//             );
//         }
//         next();
//     };
// };

module.exports = { isAuth };
