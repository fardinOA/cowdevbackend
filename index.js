const express = require("express");
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const cloudinary = require("cloudinary");
require("dotenv").config();
const cors = require("cors");
const app = express();
// app.use(express.json());
app.use(cookieParser());
const port = process.env.PORT;

app.use(
    cors({
        origin: ["http://localhost:3000", "https://cowdev.vercel.app"],
        credentials: true,
    })
);

// app.use(express.limit(100000000));

// app.use(bodyParser.json({ limit: 52428800 }));
// app.use(
//     bodyParser.urlencoded({
//         limit: 52428800,
//         extended: true,
//         parameterLimit: 50000,
//     })
// );
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ limit: "50mb" }));
mongoose
    .connect(
        `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.8a7eo.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        }
    )
    .then(() => {
        app.listen(port, () => {
            console.log(` Listening on port ${port}`);
        });
    })
    .catch((err) => {
        console.log(err);
    });

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINAY_API_SECRET,
});

const allRouter = require("./src/routes/index");

app.use("/cowdev", allRouter);

app.get("/", (req, res) => {
    res.send("Hello World!");
});
