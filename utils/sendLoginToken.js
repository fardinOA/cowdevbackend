const sendToken = async (user, statusCode, res) => {
    //create token & save in cookie
    const token = await user.getJWTToken();

    // option for cookie
    const options = {
        expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000),
        httpOnly: true,
        sameSite: "none",
        secure: true,
    };
    const secureUser = user.toObject();
    delete secureUser.password;
    JSON.stringify(secureUser);
    res.status(statusCode).cookie("cowdevtoken", token, options).json({
        success: true,
        user: secureUser,
        token,
    });
};

module.exports = sendToken;
